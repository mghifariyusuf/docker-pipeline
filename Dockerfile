FROM golang:1.15.4

RUN apt-get update\
	&& apt-get -y install zip npm python-pip\
	&& pip install xlsx2csv\
	&& npm install -g snyk

RUN curl -sSfL https://raw.githubusercontent.com/golangci/golangci-lint/master/install.sh | sh -s -- -b $(go env GOPATH)/bin v1.31.0

RUN go get -u github.com/kevinburke/go-bindata/...
ENV GO111MODULE=on
RUN go get github.com/mitchellh/golicense

RUN git config --global --add url."git@bitbucket.org:".insteadOf "https://bitbucket.org/"


